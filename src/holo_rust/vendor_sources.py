"""
This module converts downloaded tarfiles for Rust crates into
vendored sources that cargo will accept.
"""

import argparse
import hashlib
import json
import os
import shutil
import subprocess
import tempfile
import tomllib


def copy_and_checksum_file(srcfile, destfile):
    """Copy file, returning its checksum

    This is essentially `shutil.copytree()` except that it also
    returns the checksums for all the copied files.  It uses
    `shutil.copystat()` to preserve as much metadata as possible from
    the src tree in the dest tree.
    """
    h = hashlib.sha256()
    with open(srcfile, "rb") as infp:
        with open(destfile, "wb") as outfp:
            while True:
                chunk = infp.read(128 * 1024)
                if len(chunk) == 0:
                    break
                h.update(chunk)
                outfp.write(chunk)
    shutil.copystat(srcfile, destfile)
    return h.hexdigest()


def copy_and_checksum_tree(srcdir, destdir):
    """Copy tree, returning checksums of files.

    This is essentially `shutil.copytree()` except that it also
    returns the checksums for all the copied files.  It uses
    `shutil.copystat()` to preserve as much metadata as possible from
    the src tree in the dest tree.
    """
    cksums = {}

    os.makedirs(destdir, exist_ok=True)
    shutil.copystat(srcdir, destdir)
    for root, dirs, files in os.walk(srcdir):
        for d in dirs:
            srcsubdir = os.path.join(root, d)
            path = os.path.relpath(srcsubdir, start=srcdir)
            destsubdir = os.path.join(destdir, path)
            os.makedirs(destsubdir, exist_ok=True)
            shutil.copystat(srcsubdir, destsubdir)

        for f in files:
            srcfile = os.path.join(root, f)
            path = os.path.relpath(srcfile, start=srcdir)
            destfile = os.path.join(destdir, path)
            cksums[path] = copy_and_checksum_file(srcfile, destfile)

    return cksums


def substitute_line(line, ws_data):
    """Replace workspace data in a line of the toml file.

    This performs replacement on a single line of the manifest; if
    this line happens to be of the form
       <key>.workspace = true
    then we'll overwrite it with <key> = <ws_data[key]>.

    See substitute_workspace_data() for the details.

    """
    parts = line.split("=", maxsplit=2)
    if len(parts) != 2:
        return line

    key = parts[0].strip()
    value_parts = parts[1].lstrip().split("#", maxsplit=2)
    value = value_parts[0].rstrip()
    if not key.endswith(".workspace") or value != "true":
        return line

    base = key[: -len(".workspace")]
    tail = f"#{value_parts[1]}" if len(value_parts) > 1 else "\n"
    new_line = f"{base} = {ws_data[base]!r} {tail}"
    return new_line


def substitute_workspace_data(manifest, ws_data):
    """Replace workspace data inside a nested Cargo.toml file

    cargo supports defining some values at the workspace level which
    can then be re-used inside nested packages. When we vendor, we
    have to create self-contained packages, so we need to substitute
    these values. We don't have a built-in toml writer in python yet,
    so it's safest to do this at a low level, line by line.

    """
    new_lines = []
    with open(manifest, "r", encoding="utf-8") as fp:
        for line in fp.readlines():
            new_lines.append(substitute_line(line, ws_data))

    with open(manifest, "w", encoding="utf-8") as fp:
        for line in new_lines:
            fp.write(line)


def vendor_crate(src, tgt, crate_sha256s, ws_data):
    """Copy a crate to a vendored location, adding checksums"""

    substitute_workspace_data(os.path.join(src, "Cargo.toml"), ws_data)

    with open(os.path.join(src, "Cargo.toml"), "rb") as fp:
        manifest = tomllib.load(fp)

    name = manifest["package"]["name"]
    version = manifest["package"]["version"]

    cksums = copy_and_checksum_tree(src, tgt)
    with open(os.path.join(tgt, ".cargo-checksum.json"), "w", encoding="utf-8") as fp:
        print(
            json.dumps(
                {
                    "package": crate_sha256s.get((name, version)),
                    "files": cksums,
                },
                indent=4,
            ),
            file=fp,
        )


def find_manifests(path):
    """Find the top-level Cargo.toml files in the given directory

    When we expand archives, we don't know their directory structure.
    It's safest to assume only that each project directory contains a
    Cargo.toml at its root. We'll locate nested projects in the
    workspaces subsequently.
    """
    res = []
    for root, dirs, files in os.walk(path):
        if "Cargo.toml" in files:
            res.append((os.path.join(root, "Cargo.toml"), {}))
            dirs.clear()
    return res


def explode_tarfile(tarfile, outdir, crate_sha256s):
    """Extract a tarred crate, separating workspaces.

    When cargo works with vendored sources, it can no longer deal with
    virtual workspaces (i.e. directories that contain more than one
    crate). cargo's own vendoring separates out the subprojects into
    directories, which is what this does too.  Conveniently, when
    vendoring, the workspace relative paths continue to be valid.

    The argument sare:
    - `tarfile` is what to extract.
    - `outdir` is where to copy it to.
    - `crate_sha256s` is a map of sha256sums of all the crates.

    We have to pass in the crate_sha256s because they're obtained from
    data that is more difficult to obtain (it was present when the tar
    was obtained from crates.io, but it's not in the tarfiles, and it's
    always missing for git sources).
    """
    with tempfile.TemporaryDirectory() as td:
        subprocess.check_call(["tar", "xf", os.path.realpath(tarfile)], cwd=td)

        tomls = find_manifests(td)
        projs = []
        while len(tomls) > 0:
            toml, ws_data = tomls.pop(0)
            with open(toml, "rb") as fp:
                manifest = tomllib.load(fp)

            if "workspace" in manifest:
                workspace_data = {}

                if "package" in manifest["workspace"]:
                    workspace_data = manifest["workspace"]["package"]

                for proj in manifest["workspace"].get("members", []):
                    target = os.path.join(os.path.dirname(toml), proj, "Cargo.toml")

                    if os.path.exists(target):
                        tomls.append((target, workspace_data))
            else:
                p = os.path.dirname(toml)
                projs.append((p, ws_data))

        for proj, ws_data in projs:
            vendor_crate(
                proj,
                os.path.join(outdir, os.path.basename(proj)),
                crate_sha256s,
                ws_data,
            )

    return [os.path.basename(p) for p, _ in projs]


def load_crate_checksums(lockfile):
    """Load the crate SHA256s from a Cargo.lock

    This is the only place in the tree where this data is available,
    but cargo expects us to propagate it when we vendor the sources.

    """
    with open(lockfile, "rb") as fp:
        lf = tomllib.load(fp)
    cksums = {
        (m["name"], m["version"]): m["checksum"]
        for m in lf["package"]
        if "checksum" in m
    }
    return cksums


def main():
    """Entry point for this module

    We intend this to be run as a script but the pythonic way to
    package scripts now seems to be to allow setuptools to generate a
    hook script that calls out to a module function, and keep all the
    real code in site-packages.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--output-dir",
        "-o",
        required=True,
        metavar="dir",
        help="Where to explode crates to",
    )
    parser.add_argument(
        "--lock-file",
        "-L",
        required=True,
        metavar="Cargo.lock",
        help="The Cargo.lock file to use",
    )
    parser.add_argument("archive", nargs="*", help="A crate tar file to explode")
    args = parser.parse_args()

    os.makedirs(args.output_dir, exist_ok=True)
    locks = load_crate_checksums(args.lock_file)
    for arg in args.archive:
        explode_tarfile(arg, args.output_dir, locks)


if __name__ == "__main__":
    main()
