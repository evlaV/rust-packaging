"""
This module generates a placeholder PKGBUILD for a Rust crate we
wish to package for Arch. It probably makes most sense to use it
for crates that produce binaries, rather than libraries.
"""

import argparse
import hashlib
import json
import os
import subprocess
import sys
import tempfile
import tomllib
import urllib.parse


class UnhandledSource(Exception):
    """A type of crate source we have not currently implemented
    support for."""


def get_metadata(package_directory):
    """Use cargo to obtain the v1 metadata for a given Rust workspace"""

    output = subprocess.check_output(
        ["cargo", "metadata", "--locked", "--all-features", "--format-version=1"],
        cwd=package_directory,
    )

    return json.loads(output)


def get_locked_sources(metadata):
    """Obtain the rust package sources out of metadata

    This refers to the locations from which Rust packages should be
    obtained, not Rust source code itself. The result is a list of
    pairs of tarfiles and URLs from which to download them.

    We currently support only two source types:
    - crates.io
    - git sources hosted on github.com
    """
    entries = []
    redirects = set()
    for pkg in metadata.get("packages", []):
        name = pkg["name"]
        version = pkg["version"]
        source = pkg["source"]

        if source is None:
            continue

        kind, url = source.split("+", maxsplit=2)

        if kind == "registry":
            if url == "https://github.com/rust-lang/crates.io-index":
                # crates.io
                entries.append(
                    (
                        f"{name}-{version}.tar.gz",
                        f"https://crates.io/api/v1/crates/{name}/{version}/download",
                    )
                )
                redirects.add(("crates-io", ()))
            else:
                raise UnhandledSource(
                    f"Unhandled registry {url}; only crates.io is currently supported."
                )
        elif kind == "git":
            parts = urllib.parse.urlparse(url)
            if parts.netloc == "github.com" and parts.scheme == "https":
                entries.append(
                    (
                        f"{name}-{version}.tar.gz",
                        f"https://github.com{parts.path}/archive/{parts.fragment}.tar.gz",
                    )
                )
                redirects.add(
                    (
                        "git+"
                        + urllib.parse.urlunparse(
                            (
                                parts.scheme,
                                parts.netloc,
                                parts.path,
                                None,
                                parts.query,
                                None,
                            )
                        ),
                        (
                            (
                                "git",
                                urllib.parse.urlunparse(
                                    (
                                        parts.scheme,
                                        parts.netloc,
                                        parts.path,
                                        None,
                                        None,
                                        None,
                                    )
                                ),
                            ),
                            ("rev", urllib.parse.parse_qs(parts.query)["rev"][0]),
                        ),
                    )
                )
            else:
                raise UnhandledSource(
                    f"Unhandled git repository {url}; only github is currently supported."
                )
        else:
            raise UnhandledSource(
                f"Unhandled source kind {kind}; only registry and git are supported."
            )

    mapped = {}
    for tarfile, url in entries:
        mapped.setdefault(url, []).append(tarfile)

    entries = []
    for url, tarfiles in mapped.items():
        tarfiles.sort()
        entries.append((tarfiles[0], url))

    redirects = list(redirects)
    redirects.sort()

    return entries, redirects


def make_github_source(package):
    """Automatically derive a tarfile for the crate being packaged.

    This only works for crates whose metada lists them as being hosted
    on github (we examine the crate `repository` field to determine
    this.
    """
    name = package["name"]
    version = package["version"]
    url = package["repository"]
    parts = urllib.parse.urlparse(url)
    if parts.netloc == "github.com" and parts.scheme == "https":
        url += f"/archive/v{version}.tar.gz"
        return [(f"{name}-{version}.tar.gz", url)]
    return []


def generate_shas(sources, verbose=True):
    """Pull list of sources and SHA256 them.

    This is done to automatically populate the sha256sums array in the
    resulting PKGBUILD. If verbose is set, there will be some progress
    output (since downloading and sha256 summing every dependency can
    be quite time consuming).
    """
    shas = []
    for tarfile, url in sources:
        if verbose:
            print(f"Downloading {tarfile} from {url}", file=sys.stderr)
            sys.stderr.flush()
        with tempfile.TemporaryDirectory() as td:
            subprocess.check_call(
                ["curl", "-s", "-L", "-o", os.path.join(td, tarfile), url]
            )
            h = hashlib.sha256()
            with open(os.path.join(td, tarfile), "rb") as fp:
                while True:
                    chunk = fp.read(16384)
                    if len(chunk) == 0:
                        break
                    h.update(chunk)
        shas.append(h.hexdigest())
    return shas


def parse_manifest(manifest):
    """Parse Cargo.toml"""
    with open(manifest, "rb") as fp:
        return tomllib.load(fp)


def print_shell_list(fp, var, values):
    """Pretty print a list in bash format

    This matches expectations for formatting in a PKGBUILD, when a
    long array (e.g. of sha256sums or sources) is to be printed.
    There is one element per line, indented to match.
    """
    if len(values) == 0:
        print(f"{var}=()", file=fp)
        return

    if len(values) == 1:
        print(f"{var}=('{values[0]}')", file=fp)
        return

    print(f"{var}=('{values[0]}'", file=fp)
    pad = " " * (len(var) + 2)
    for v in values[1:-1]:
        print(f"{pad}'{v}'", file=fp)
    print(f"{pad}'{values[-1]}')", file=fp)


def print_pkgbuild(fp, manifest, sources, shas, redirects):
    """Print a PKGBUILD to stdout

    This is not a complete PKGBUILD, because the `package()`
    function is a stub, however it does contain all the necessary
    sources and shasums.

    The arguments are:
    - `manifest`: The parsed Cargo.toml file
    - `sources`: The sources parsed from the metadata
    - `shas`: A matching array of sha256sums.
    - `redirects`: The source replacements needed during build to use
      vendored sources.
    """

    print("# This file was partially generated by holo-make-rust-pkgbuild.", file=fp)
    print("# To update it, run holo-make-rust-pkgbuild on the crate sources", file=fp)
    print("# and then manually merge the resulting PKGBUILD with this file.", file=fp)
    print(file=fp)

    pkg = manifest.get("package", {})

    for author in pkg.get("authors", []):
        print(f"# Maintainer (upstream): {author}", file=fp)

    name = pkg["name"]
    version = pkg["version"]
    desc = pkg.get("description")
    homepage = pkg.get("homepage")
    crate_license = pkg.get("license")
    print(f"pkgname={name}", file=fp)
    print(f"pkgver={version}", file=fp)
    print("pkgrel=1", file=fp)
    print(
        "arch=('i686' 'pentium4' 'x86_64' 'arm' 'armv7h' 'armv6h' 'aarch64')", file=fp
    )
    if desc:
        print(f"pkgdesc='{desc}'", file=fp)
    if homepage:
        print(f"url='{homepage}'", file=fp)

    print_shell_list(
        fp,
        "source",
        [f"{tf}::{url}" for tf, url in sources],
    )

    print_shell_list(fp, "makedepends", ["cargo", "holo-rust-packaging-tools"])
    print(f"license=('{crate_license}')", file=fp)

    print_shell_list(fp, "sha256sums", shas)

    print(
        """\

prepare() {
  cd "$srcdir"

  holo-vendor-rust-sources -o vendored -L "$pkgname-$pkgver/Cargo.lock" *.tar.gz

  cd "$pkgname-$pkgver"

  mkdir -pv .cargo

  cat << EOF >> .cargo/config.toml
""",
        file=fp,
    )

    for name, keys in redirects:
        print(f'    [source."{name}"]', file=fp)
        for k, v in keys:
            print(f'    {k} = "{v}"', file=fp)
        print('    replace-with = "vendored-sources"', file=fp)
        print(file=fp)

    print("    [source.vendored-sources]", file=fp)
    print('    directory = "${srcdir}/vendored"', file=fp)
    print(
        """\
EOF
}

build () {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ $CARCH != x86_64 ]]; then
    export CARGO_PROFILE_RELEASE_LTO=off
  fi

  cargo build --offline --features "${_features:-}" --release --target-dir target
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
}

""",
        file=fp,
    )


def process_crate(target, fp, verbose=True):
    """Analyse a crate and produce an initial PKGBUILD

    The crate must be checked out locally in `target`. We'll write the
    initial PKGBUILD to `fp`. Note that the PKGBUILD is incomplete,
    because it's not clear how to publish an arbitary crate (so the
    `package()` routine is empty).
    """
    try:
        manifest = parse_manifest(os.path.join(target, "Cargo.toml"))

        m = get_metadata(target)
        sources = make_github_source(manifest["package"])
        dep_sources, redirects = get_locked_sources(m)
        sources.extend(dep_sources)

        shas = generate_shas(sources, verbose)

        print_pkgbuild(fp, manifest, sources, shas, redirects)
    except UnhandledSource as e:
        print(f"Error processing {target}: {e}")
        sys.exit(1)


def main():
    """Entry point for this module

    We intend this to be run as a script but the pythonic way to
    package scripts now seems to be to allow setuptools to generate a
    hook script that calls out to a module function, and keep all the
    real code in site-packages.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-q",
        "--quiet",
        dest="verbose",
        action="store_false",
        help="Don't produce tracing output while calculating SHAs",
    )
    parser.add_argument("target", help="directory containing crate to process")
    args = parser.parse_args()

    process_crate(args.target, sys.stdout)


if __name__ == "__main__":
    main()
