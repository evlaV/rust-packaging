.PHONY: all build install clean

all: build

build: $(SOURCES)
	python -m build --wheel --no-isolation

install: build
	python -m installer --destdir="$(DESTDIR)" dist/*.whl

clean:
	rm -rf build/ dist/
