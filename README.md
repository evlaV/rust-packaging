# Holo Rust Packaging Tools

This repository contains two Python tools to help with packaging Rust
applications in the approved way.

To use, invoke:

```bash
holo-rust-make-pkgbuild path/to/crate/source > PKGBUILD
```
 
This will yield a stub PKGBUILD in the current directory. You'll need to
complete it with any missing details, but it will contain:
- the full sources list
- the full sha256sum array
- some basic metadata from the crate:
   - name
   - version
   - license
   - description
   - makedepends
- a complete `prepare()` function to process the sources (using
  `holo-rust-vendor-sources`, which is also in this repo).
- a minimal `build()` function, which you may need to extend.
- a no-op `package()` function that you will need to complete.

If you are updating an existing PKGBUILD, you probably want to
generate the new PKGBUILD alongside the old one, and copy over any
existing customisations. Unfortunately, there's no simple way to do
this automatically, given that a PKGBUILD is just a bash script.

# License

MIT
